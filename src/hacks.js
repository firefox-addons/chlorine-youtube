/* global document window */

setInterval(() => {
  const ads = !!document.getElementsByClassName('ytp-ad-text')[0] || !!document.getElementsByClassName('videoAdUi')[0];

  const vid = document.getElementsByTagName('video')[0];
  if (ads && vid) {
    vid.currentTime = vid.duration;
  }

  const button = document.getElementsByClassName('videoAdUiSkipButton')[0];
  if (button) {
    button.click();
  }
}, 250);
